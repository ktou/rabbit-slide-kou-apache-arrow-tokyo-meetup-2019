= Apache Arrow 2019

2019年12月時点のApache Arrowの最新情報を紹介します。

== ライセンス

=== スライド

CC BY-SA 4.0

原著作者名は以下の通りです。

  * 須藤功平（またはKouhei Sutou）

=== 画像

==== クリアコードのロゴ

CC BY-SA 4.0

原著作者：株式会社クリアコード

ページヘッダーで使っています。

== 作者向け

=== 表示

  rake

=== 公開

  rake publish

== 閲覧者向け

=== インストール

  gem install rabbit-slide--apache-arrow-tokyo-meetup-2019

=== 表示

  rabbit rabbit-slide--apache-arrow-tokyo-meetup-2019.gem

